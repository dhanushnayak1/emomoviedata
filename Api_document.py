from ast import keyword
from cgitb import text
import pandas as pd
from flask import Flask,request,jsonify,Blueprint
from flask_restplus import Api,fields,reqparse,Resource
import logging
from Movie_Suggestion.suggestion import movie_recommandation
document = Blueprint('api', __name__,url_prefix='/api')

"""
logger = logging.getLogger('api')
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter(f'%(asctime)s %(levelname)s %(name)s %(threadName)s : %(message)s')
file_handler = logging.FileHandler("log/api.log")
file_handler.setFormatter(formatter)
logger.addHandler(file_handler)"""
log = logging.getLogger('app')
log.setLevel(logging.DEBUG)
formatter = logging.Formatter(f'%(asctime)s %(levelname)s %(name)s %(threadName)s : %(message)s')
file_handler = logging.FileHandler("log/app.log")
file_handler.setFormatter(formatter)
log.addHandler(file_handler)

api = Api(document,version='1.0',title="Api for Movie Suggestion",description="Team Amrutha. ")
movie_data= api.namespace("movie")

upload_parser = api.parser()
upload_parser.add_argument('emotion',required=False,type=str,help='Person emotion is helps to give recommandation based on emotion and search')
upload_parser.add_argument("movie_name",required=False,type=str,help='Search movie by name')
upload_parser.add_argument("limit",required=False,type=int,help='Number of records to retrive')
upload_parser.add_argument("content",required=False,type=bool,help='Return based on content match')
upload_parser.add_argument("keyword",required=False,type=bool,help='Return based on Keyword match')

dataset = pd.read_csv("./movie_dataset.csv")

@movie_data.route("/search_by_name/")
@movie_data.doc(responses={ 200: 'OK', 404: 'Not able to post'})
@movie_data.expect(upload_parser)
class MovieData(Resource):
    def post(self):
        try:
            args = upload_parser.parse_args()
            name = args.get('movie_name',None)
            emotion= args.get('emotion',None)
            limit = args.get('limit',None)
            if limit is None: limit = 100
            mr = movie_recommandation(df=dataset,emotion=emotion)
            data = mr.get_close_by_name(x=name,limit=limit)
            log.info(f'search = {name}, emotion = {emotion}, limit = {limit},near_by = {data.iloc[0].title}, method = search_by_name')
            return {"parameter":{"name":name,"emotion":emotion,"limit":limit},"data":data.title.to_list()}
        except Exception as e:
            log.error(e.__doc__)
            pass

@movie_data.route("/get_content_recommandation/")
@movie_data.doc(responses={ 200: 'OK', 404: 'Not able to post'})
@movie_data.expect(upload_parser)
class MovieData_v2(Resource):
    def post(self):
        
            args = upload_parser.parse_args()
            name = args.get('movie_name',None)
            emotion= args.get('emotion',None)
            limit = args.get('limit',None)
            content = args.get('content',None)
            keyword = args.get("keyword",None)
            if content is None and keyword is None:
                content =  True
            if limit is None: limit = 100
            mr = movie_recommandation(df=dataset,emotion=emotion)
            data = mr.get_recommandation(x=name,limit=limit,content=True)
            if content==True:method ='content'
            if keyword==True:method='keyword'
            log.info(f'search = {name}, emotion = {emotion}, limit = {limit},near_by = {data[1]}, method = search_by_{method}')
            return {"parameter":{"name":name,"emotion":emotion,"content":content,"keyword":keyword,"limit":limit},"data":data[0].to_html(),"near_search":data[1]}
        


@movie_data.route("/get_keyword_recommandation/")
@movie_data.doc(responses={ 200: 'OK', 404: 'Not able to post'})
@movie_data.expect(upload_parser)
class MovieData_v3(Resource):
    def post(self):
        try:
            args = upload_parser.parse_args()
            name = args.get('movie_name',None)
            emotion= args.get('emotion',None)
            limit = args.get('limit',None)
            content = args.get('content',None)
            keyword = args.get("keyword",None)
            if content is None and keyword is None:
                keyword =  True
            if limit is None: limit = 100
            mr = movie_recommandation(df=dataset,emotion=emotion)
            data = mr.get_recommandation(x=name,limit=limit,content=True)
            if content==True:method ='content'
            if keyword==True:method='keyword'
            log.info(f'search = {name}, emotion = {emotion}, limit = {limit},near_by = {data[1]}, method = search_by_{method}')
            return {"parameter":{"name":name,"emotion":emotion,"content":content,"keyword":keyword,"limit":limit},"data":data[0].to_html(),"near_search":data[1]}
        except Exception as e:
            log.error(e.__doc__)
            pass
